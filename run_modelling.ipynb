{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "5e3f24c2",
   "metadata": {},
   "source": [
    "# System preparation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c5a5b4a",
   "metadata": {},
   "source": [
    "Starting from the X-ray structure (all-atom) of the carbonyl reductase (CpRCR) with substrates 2-pentanone docked in the active site, you will generate the force field parameters for describing the interactions between catalytic zinc and its coordinating residues. The force field parameters for NADH and structural zinc will then be prepared. This structure will be solvated by a water box and will be neutralized by adding counter ions. A “tleap.in” file will be created as an input file of tleap software in the Amber suite for the whole system modelling described above. Finally, the system will be used in the following steps for the molecular dynamic simulation."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a437df1e",
   "metadata": {},
   "source": [
    "**1.** Download the whole repository from this link (https://gitlab.com/BiocompZhao/cprcr_simulations) as a compressed file or, if you use git, clone the repository with the next command: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "938bcacb",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "git clone https://gitlab.com/BiocompZhao/cprcr_simulations.git"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fe02ad65",
   "metadata": {},
   "source": [
    "**Alternative step:** If there is any problem for access to the repository, you can download the permanent copy at Zenodo under the DOI XXXXXX."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b9dcddda",
   "metadata": {},
   "source": [
    "**2.** Once the repository has been decompressed as a folder in your computer, open a bash terminal and change the working directory to there with the following command, where PATH is the location in your computer where you saved the content of the repository:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "22643794",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cd PATH/cprcr_simulations"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "54616841",
   "metadata": {},
   "source": [
    "**3.** Create a working director named as you prefer (“RUN” in this example):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "ee523624",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "mkdir RUN"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3797358a",
   "metadata": {},
   "source": [
    "**4.** Preparation of PDB file for modelling (protonation state calculation)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cc9df4f4",
   "metadata": {},
   "source": [
    "&emsp;**a.** Create a new folder named “preparePDB” in “RUN” and change the working directory to that folder:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "7dd5c8cc",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cd RUN\n",
    "mkdir preparePDB\n",
    "cd preparePDB"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "04d40bdd",
   "metadata": {},
   "source": [
    "&emsp;**b.** Copy the original PDB file “3wle_chain_A_initial.pdb” from the folder “example_files” into the folder “preparedPDB”. Copy the substrate 2-pentanone, protein, NADH cofactor, structural zinc, and catalytic zinc to individual files:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "19e54fc7",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cp ../../example_files/3wle_chain_A_initial.pdb ./\n",
    "awk '($1 == \"ATOM\" && $4 != \"NAH\" && $4 != \"TOA\" && $4 !=\"ZN\"){print}END{print \"TER\"}' 3wle_chain_A_initial.pdb > 3wle_chain_A_TOA.pdb\n",
    "awk '($4 == \"ZN\" && $6 == 402){print} END{print \"TER\"}' 3wle_chain_A_initial.pdb > ZN.pdb\n",
    "awk '($4 == \"ZN\" && $6 == 403){print} END{print \"TER\"}' 3wle_chain_A_initial.pdb > ZN-stru.pdb\n",
    "awk '($4 == \"TOA\"){print}END{print \"TER\"}' 3wle_chain_A_initial.pdb > TOA.pdb\n",
    "awk '($4 == \"NAH\"){print}END{print \"TER\"}' 3wle_chain_A_initial.pdb > NAH.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b01232f8",
   "metadata": {},
   "source": [
    "&emsp;**Note:** In this protocol, “3wle_chain_A_initial.pdb” corresponds to the X-ray structure of CpRCR with substrate docked in the active site."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cffd077b",
   "metadata": {},
   "source": [
    "&emsp;**c.** Determine the protonation states for standard residues of the protein. Here we use the H++ web server13 (http://newbiophysics.cs.vt.edu/H++/) to calculate the protonation states. Upload the file \"3wle_chain_A_TOA.pdb” to the server and the server will give two results (e.g. “0.15_80_10_pH6.5_3wle_chain_A_TOA.top” and “0.15_80_10_pH6.5_3wle_chain_A_TOA.crd”), download them into this folder. Issue the “ambpdb” commands to generate the result PDB file “3wle_chain_A_Hpp.pdb”: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "024769ae",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "ambpdb -p 0.15_80_10_pH6.5_3wle_chain_A_TOA.top -c 0.15_80_10_pH6.5_3wle_chain_A_TOA.crd > 3wle_chain_A_Hpp.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fabbcf32",
   "metadata": {},
   "source": [
    "&emsp;**d.** Make sure the His49 is protonated. The residues Cys44, His65, and Asp154 coordinate with the catalytic zinc, and the residues Cys95, Cys98, Cys101, and Cys109 coordinate with the structural zinc. Thus we will delete the hydrogen atom bonded to the sulfur or nitrogen atom of these residues to make them coordinate with the catalytic or structural zinc. we will generate a new PDB file “3wle_chain_A_Hpp_fixed.pdb”:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "d92c2051",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "sed -e 's/HI.    48/HIP    48/g' 3wle_chain_A_Hpp.pdb > 3wle_chain_A_Hpp_fixed.pdb\n",
    "sed -i '/HG  CYS    43/d;/HE2 HI.    64/d;/HG  CYS    94/d;/HG  CYS    97/d;/HG  CYS   100/d;/HG  CYS   108/d' 3wle_chain_A_Hpp_fixed.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4a9c8b79",
   "metadata": {},
   "source": [
    "&emsp;**e.** Concatenate all individual files to a single file “3wle_chainA_21ar_H.pdb” in the order of substrate, protein, NADH, catalytic zinc, and structural zinc and we will reorder the PDB ATOM records using “pdb4amber” of AMBER program suite:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "8bd8740d",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cat TOA.pdb 3wle_chain_A_Hpp_fixed.pdb NAH.pdb ZN.pdb ZN-stru.pdb |pdb4amber | sed -e 's/HETATM/ATOM  /g' > 3wle_chainA_21ar_H.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20b5000a",
   "metadata": {},
   "source": [
    "&emsp;**f.** We will change the name of residue His49 (because it is pronated) to “HIP” (AMBER naming convention), and change the name of residues Cys44 and His65 to “CYM” and “HID”, respectively. We also Change the name of residues Cys95, Cys98, Cys101, and Cys109 to “CY1” defined by the ZAFF force field, and the name of structural zinc to “ZN2” defined by the ZAFF force field."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "a0e96b51",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "sed -i 's/HI.    49/HIP    49/g' 3wle_chainA_21ar_H.pdb\n",
    "sed -i 's/CYS    44/CYM    44/g;s/HI.    65/HID    65/g;s/CYS    95/CY1    95/g;s/CYS    98/CY1    98/g;s/CYS   101/CY1   101/g;s/CYS   109/CY1   109/g;s/ZN  . 339/ZN2   339/g' 3wle_chainA_21ar_H.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "479cb133",
   "metadata": {},
   "source": [
    "**5.** We will use the “MCPB.py” script of AMBER to calculate the force field parameters for catalytic zinc and its coordinating residues:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df56cf0a",
   "metadata": {},
   "source": [
    "&emsp;**a.** Create a new directory “MCPB” in the directory “RUN” and change directory to “MCPB”:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "69ebfde6",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cd ../\n",
    "mkdir MCPB\n",
    "cd MCPB/"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2029ce89",
   "metadata": {},
   "source": [
    "&emsp;**b.** We will use “antechamber” of AMBER to generate mol2 files “TOA.mol2” for substrate 2-heptanone. Here we are using the AM1-BCC charge method to generate the charges. The substrate has a charge of 0. We use GAFF atom types for the substrate."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "9be5e0b6",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "antechamber -fi pdb -fo mol2 -i ../preparePDB/TOA.pdb -o TOA.mol2 -c bcc -pf y -nc 0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6b69a51f",
   "metadata": {},
   "source": [
    "&emsp;**c.** We will use “parmchk2” of AMBER to obtain the “frcmod” file “TOA.frcmod” for the substrate:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "ba513dfc",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "parmchk2 -i TOA.mol2 -o TOA.frcmod -f mol2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb9d3d6d",
   "metadata": {},
   "source": [
    "&emsp;**d.** We will use the “metalpdb2mol2.py” script to prepare the mol2 file for the catalytic zinc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "aeaba1b7",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "wget http://ambermd.org/tutorials/advanced/tutorial20/files/mcpbpy/metalpdb2mol2.py\n",
    "python metalpdb2mol2.py -i ../preparePDB/ZN.pdb -o ZN.mol2 -c 2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0385b9ca",
   "metadata": {},
   "source": [
    "&emsp;**e.** We will generate the PDB, Gaussian, and fingerprint modelling files:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "19c0293c",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "id=`awk '($4==\"ZN\"){print $2}' ../preparePDB/3wle_chainA_21ar_H.pdb`\n",
    "sed -e 's/ID/'${id}'/' ../../MCPB/MCPB.in > MCPB.in\n",
    "MCPB.py -i MCPB.in -s 1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1aba28d6",
   "metadata": {},
   "source": [
    "&emsp;**Note:** Note: Here, the parameters for the standard residues of protein are obtained from ff14SB force field."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a91f73b6",
   "metadata": {},
   "source": [
    "&emsp;**f.** We will perform the Gaussian calculations for the catalytic zinc and its coordinating residues:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "805f9e80",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "sed -i 's/%NProcShared=2/%NProcShared=16/' 3wle_chainA_21ar_H_small_opt.com\n",
    "g16 < 3wle_chainA_21ar_H_small_opt.com > 3wle_chainA_21ar_H_small_opt.log\n",
    "sed -i 's/%NProcShared=2/%NProcShared=16/' 3wle_chainA_21ar_H_small_fc.com\n",
    "g16 < 3wle_chainA_21ar_H_small_fc.com > 3wle_chainA_21ar_H_small_fc.log\n",
    "formchk 3wle_chainA_21ar_H_small_opt.chk > 3wle_chainA_21ar_H_small_opt.fchk\n",
    "sed -i 's/%NProcShared=2/%NProcShared=16/' 3wle_chainA_21ar_H_large_mk.com\n",
    "g16 < 3wle_chainA_21ar_H_large_mk.com > 3wle_chainA_21ar_H_large_mk.log"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "043fca6b",
   "metadata": {},
   "source": [
    "&emsp;**g.** We will use the Seminario method to generate the force field parameters of the catalytic zinc site to the file “3wle_chainA_21ar_H_mcpbpy.frcmod”:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "a36d5dd0",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "MCPB.py -i MCPB.in -s 2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5064995e",
   "metadata": {},
   "source": [
    "&emsp;**h.** We will perform the MK RESP charge fitting using the ChgModB scheme, and generate the mol2 files for the catalytic zinc site residues. This step will generate five mol2 files for the residues in catalytic zinc site: “TA1.mol2”, “CM1.mol2”, “HD1.mol2”, “AP1.mol2”, and “ZN1.mol2”."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "eae70353",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "MCPB.py -i MCPB.in -s 3"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d89e157",
   "metadata": {},
   "source": [
    "&emsp;**i.** We will generate the input file of “tleap” program for the catalytic zinc site. This step will obtain a new PDB file “3wle_chainA_21ar_H_mcpbpy.pdb” with modified names of catalytic zinc site residues and a “tleap” input file “3wle_chainA_21ar_H_tleap.in”."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "aafa39af",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "MCPB.py -i MCPB.in -s 4"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a298a184",
   "metadata": {},
   "source": [
    "**6.** We will use “tleap” program to generate the topology and coordinate files. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b92f3fc2",
   "metadata": {},
   "source": [
    "&emsp;**a.** Create a new directory “LeapModelling” in folder “RUN” and copy the files generated by “MCPB.py” into “LeapModelling”:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "1df61654",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cd ../\n",
    "mkdir LeapModelling\n",
    "cd LeapModelling/\n",
    "cp ../MCPB/TA1.mol2 ../MCPB/CM1.mol2 ../MCPB/HD1.mol2 ../MCPB/AP1.mol2 ../MCPB/ZN1.mol2 ./\n",
    "cp ../MCPB/TOA.frcmod ./\n",
    "cp ../MCPB/3wle_chainA_21ar_H_mcpbpy.frcmod ./\n",
    "cp ../MCPB/3wle_chainA_21ar_H_mcpbpy.pdb ./"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "43e09249",
   "metadata": {},
   "source": [
    "&emsp;**b.** We will copy the force field parameters files for the structural zinc site residues from the “FF” folder into this folder. These parameters are obtained from ZAFF force field parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "4df1e0e5",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cp ../../FF/ZN2-struc.mol2 ./\n",
    "cp ../../FF/ZN2-struc.frcmod ./\n",
    "cp ../../FF/CY1.mol2 ./"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "307c7572",
   "metadata": {},
   "source": [
    "&emsp;**c.** We will copy the force field parameter files for the cofactor NADH developed by Ryde into this folder:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "2fe6f79d",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cp ../../FF/nad.frcmod ./\n",
    "cp ../../FF/nadh.prep ./"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "71341091",
   "metadata": {},
   "source": [
    "&emsp;**d.** We will modify the input file of “tleap”, since the structural zinc site residues are not standard residues defined by AMBER. The atom types of residues in structural zinc site are not recognized by “tleap”, and the connection to other standard residues will not be generated. Thus, we will define new atom types for structural zinc site residues and connect the residues as necessary. This step will get a new tleap input file “tleap.in”: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "20d4f540",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "sed -e '/addAtomTypes/a\\{ \"ZN\"  \"Zn\" \"sp3\" }\\n{ \"S1\"  \"S\" \"sp3\" }' -e '/ZN1/a\\ZN2 = loadmol2 ZN2-struc.mol2\\nCY1 = loadmol2 CY1.mol2\\nloadAmberPrep nadh.prep\\nloadamberparams nad.frcmod\\nloadAmberParams ZN2-struc.frcmod' -e '/mol = loadpdb/a\\bond mol.95.SG mol.339.ZN\\nbond mol.98.SG mol.339.ZN\\nbond mol.101.SG mol.339.ZN\\nbond mol.109.SG mol.339.ZN\\nbond mol.94.C mol.95.N\\nbond mol.95.C mol.96.N\\nbond mol.97.C mol.98.N\\nbond mol.98.C mol.99.N\\nbond mol.100.C mol.101.N\\nbond mol.101.C mol.102.N\\nbond mol.108.C mol.109.N\\nbond mol.109.C mol.110.N' ../MCPB/3wle_chainA_21ar_H_tleap.in > tleap.in"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cc379286",
   "metadata": {},
   "source": [
    "&emsp;**e.** We will use a hybrid bonded/restrained nonbonded model for the catalytic zinc site residues by treating the coordinating bonds between substrate carbonyl oxygen and catalytic zinc with a restrainted nonbonded model while the others in a bonded model. Thus, we delete the “bond” command for the substrate carbonyl oxygen and the ZN1 in the file “tleap.in” generated by MCPB.py, and a new restraint file will be generated to treat this bond in a nonbonded way (Step 9):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "46355909",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "sed -i '/bond mol.1.O1 mol.338.ZN/d' tleap.in"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "33306c64",
   "metadata": {},
   "source": [
    "**7.** We will use “tleap” program to generate the topology file “3wle_chainA_21ar_H_solv.prmtop” and coordinate file “3wle_chainA_21ar_H_solv.inpcrd”:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "68a31cb0",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "tleap -s -f tleap.in"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Amber",
   "language": "python",
   "name": "amber"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
