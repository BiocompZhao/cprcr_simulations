#!/bin/bash

prmtop=$1
trajectory=$2
outprefix=$3
nClusters=$4

cpptraj <<EOF > Cluster.log
parm ${prmtop}
trajin ${trajectory}
cluster ${outprefix} clusters ${nClusters} "(:1@C4 |:44@CA,SG |:46@CA,OG |:65@NE2,CA|:154@CA,OD2 |:339@C'N1,C4N|:337@ZN)" sieve 5 out ${outprefix}_cnumvtime.dat summary ${outprefix}_summary.out repout ${outprefix} repfmt restart
EOF

