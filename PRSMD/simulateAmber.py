from openmm.app import *
from openmm import *
from openmm.unit import *
from sys import stdout
import sys


_, prmtop, inpcrd, restraints, out = sys.argv

with open(restraints, mode='r') as f:
    line = f.readline()
    id1, id2 = [int(i) for i in line.split()[2].split(',')]
    for i in line.split():
        if i.startswith('r2='):
            minFx=float(i[3:-1])
        elif i.startswith('rk2='):
            frc = float(i[4:-1])
h=HarmonicBondForce()
h.addBond(id1-1,id2-1,minFx/10,(100*4.184)*frc)

#print(h.getBondParameters(0))
#print(minFx,type(minFx),frc,type(frc))

prmtop = AmberPrmtopFile(prmtop)
inpcrd = AmberInpcrdFile(inpcrd)
system = prmtop.createSystem(nonbondedMethod=PME, nonbondedCutoff=1*nanometer,
        constraints=HBonds)
system.addForce(h)
#print(system.getForces()[-1].getBondParameters(0))
integrator = LangevinMiddleIntegrator(300*kelvin, 1/picosecond, 0.001*picoseconds)
simulation = Simulation(prmtop.topology, system, integrator)
simulation.context.setPositions(inpcrd.positions)
simulation.context.setVelocities(inpcrd.velocities)
if inpcrd.boxVectors is not None:
    simulation.context.setPeriodicBoxVectors(*inpcrd.boxVectors)
#i=1
#for atom in prmtop.topology.atoms():
#   print(i, atom.name, atom.index, atom.element, atom.residue)
#   i+=1
#simulation.minimizeEnergy()
simulation.reporters.append(DCDReporter(out, 1000))
simulation.reporters.append(StateDataReporter(stdout, 1000, step=True,
        potentialEnergy=True, temperature=True))
simulation.step(1000000)


