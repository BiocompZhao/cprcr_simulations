#!/bin/bash
mkdir LeapModelling
cd LeapModelling/
cp ../MCPB/TA1.mol2 ../MCPB/CM1.mol2 ../MCPB/HD1.mol2 ../MCPB/AP1.mol2 ../MCPB/ZN1.mol2 ./
cp ../MCPB/TOA.frcmod ./
cp ../MCPB/3wle_chainA_21ar_H_mcpbpy.frcmod ./
cp ../MCPB/3wle_chainA_21ar_H_mcpbpy.pdb ./
cp ../../FF/ZN2-struc.mol2 ./
cp ../../FF/ZN2-struc.frcmod ./
cp ../../FF/CY1.mol2 ./
cp ../../FF/nad.frcmod ./
cp ../../FF/nadh.prep ./
sed -e '/addAtomTypes/a\{ "ZN"  "Zn" "sp3" }\n{ "S1"  "S" "sp3" }' -e '/ZN1/a\ZN2 = loadmol2 ZN2-struc.mol2\nCY1 = loadmol2 CY1.mol2\nloadAmberPrep nadh.prep\nloadamberparams nad.frcmod\nloadAmberParams ZN2-struc.frcmod' -e '/mol = loadpdb/a\bond mol.95.SG mol.339.ZN\nbond mol.98.SG mol.339.ZN\nbond mol.101.SG mol.339.ZN\nbond mol.109.SG mol.339.ZN\nbond mol.94.C mol.95.N\nbond mol.95.C mol.96.N\nbond mol.97.C mol.98.N\nbond mol.98.C mol.99.N\nbond mol.100.C mol.101.N\nbond mol.101.C mol.102.N\nbond mol.108.C mol.109.N\nbond mol.109.C mol.110.N' ../MCPB/3wle_chainA_21ar_H_tleap.in > tleap.in
sed -i '/bond mol.1.O1 mol.338.ZN/d' tleap.in
