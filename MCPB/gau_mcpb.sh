#!/bin/sh
sed -i 's/%NProcShared=2/%NProcShared=16/' 3wle_chainA_21ar_H_small_opt.com
g09 < 3wle_chainA_21ar_H_small_opt.com > 3wle_chainA_21ar_H_small_opt.log
sed -i 's/%NProcShared=2/%NProcShared=16/' 3wle_chainA_21ar_H_small_fc.com
g09 < 3wle_chainA_21ar_H_small_fc.com > 3wle_chainA_21ar_H_small_fc.log

formchk 3wle_chainA_21ar_H_small_opt.chk > 3wle_chainA_21ar_H_small_opt.fchk
sed -i 's/%NProcShared=2/%NProcShared=16/' 3wle_chainA_21ar_H_large_mk.com
g09 < 3wle_chainA_21ar_H_large_mk.com > 3wle_chainA_21ar_H_large_mk.log

