#!/bin/bash

Prmtop=$1
Traj=$2
FrameN=$3
rstName=$4

cpptraj <<EOF > cpptraj.log
parm ${Prmtop}
trajin ${Traj}
autoimage
trajout ${rstName} restart onlyframes ${FrameN}
go
EOF

