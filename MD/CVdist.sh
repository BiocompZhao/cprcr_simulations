#!/bin/bash

Prmtop=$1
Traj=$2
out=$3

cpptraj <<EOF > cpptraj.log
parm ${Prmtop}
trajin ${Traj}
distance Ca_C5 :262@CA :TA1@C5 out ${out} time 0.002
go
EOF


