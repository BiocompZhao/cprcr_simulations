import sys
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np

_, dist, outf = sys.argv
dat = np.genfromtxt(dist, dtype=[('Time', float), ('Distance', float)])
fnt = {
        "family": "serif",
        "stretch": "condensed",
        }
mpl.rc("font", **fnt)
mpl.rc("pdf", fonttype=42)

fig, ax = plt.subplots(figsize=(3.15,2.36))
ax.plot(dat['Time'][:2500], dat['Distance'][:2500], lw = 0.75)
ax.set_xlim(0,5)
ax.xaxis.set_major_locator(ticker.LinearLocator(6))
ax.xaxis.set_minor_locator(ticker.LinearLocator(26))
ax.yaxis.set_major_locator(ticker.MultipleLocator(2.0))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(1.0))
ax.set_xlabel("Time (ns)")
ax.set_ylabel(r"$Distance_{CA-C5} (\AA)$")
plt.savefig(outf + '.png', dpi=300, bbox_inches='tight')
plt.savefig(outf + '.pdf', dpi=300, bbox_inches='tight')
