import re
import sys

#Author: S. Luo
_, filepath  = sys.argv

elementTable = ['pass','H','He','Li','Be','B','C','N','O','F','Ne','Na','Mg','Al','Si','P','S','Cl','Ar','K','Ca',\
'Sc','Ti','V','Cr','Mn','Fe','Co','Ni','Cu','Zm','Ga','Ge','As','Se','Br','Kr','Rb','Sr','Y','Zr','Nb',\
'Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn','Sb','Te','I','Xe','Cs','Ba',\
'La','Ce','Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu','Hf',\
'Ta','W','Re','Os','Ir','Pt','Au','Hg','Tl','Pb','Bi','Po','At','Rn']
PDBinfo = []
Geo = 0
i=0
points = []
Heads = []

with open(filepath, mode='r') as f:
    point = []
    Head = []
    pn = 1
    pointn = 0
    settings = []
    layers = []
    commandFlag = 0
    while True:
        line = f.readline()
        if line == '':
            if len(Head) == len(point) - 1:
                point.pop()
            points.append(point)
            Heads.append(Head)
            #print(settings, command,layers)
            break
        if re.match(r' %chk| %lindaworkers| %mem| %nproc', line, re.IGNORECASE):
            settings.append(line.strip())
        if re.match(r' #', line):
            command = line.strip()
            commandFlag = 1
            continue
        if commandFlag == 1 and ' -----' not in line:
            command = command + line.strip()
            continue
        if commandFlag == 1 and ' -----' in line:
            commandFlag = 0
        if 'PDBName' in line and 'ResName' in line and 'ResNum' in line or re.match(r' \S+\s+(0|-1)\s+-?\d*\.\d*\s+-?\d*\.\d*\s+-?\d*\.\d*\s+(H|M|L)\s*.*\n', line):
            PDBinfo.append('{:53s} {:>2s}     '.format(line.split()[0], line.split()[1]))
            if len(line.split()) == 6:
                layers.append(' {:2s} '.format(line.split()[-1]))
            else:
                layers.append(' {:2s} {:8s} {:5s}'.format(*line.split()[5:8]))
            continue
        if 'Input orientation' in line or 'Standard orientation' in line:
            Geo = 1
            coords = []
            continue
        if Geo == 1 and re.match(r'\s+\d+\s+\d+\s+\d+\s+', line):
            matchObj = re.match(r'^\s+\d+\s+(?P<atnum>\d+)\s+\d+\s+(?P<x>-?\d+\.\d+)\s+(?P<y>-?\d+\.\d+)\s+(?P<z>-?\d+\.\d+)\s*',line)
            coords.append((elementTable[int(matchObj.group(1))] if len(PDBinfo) == 0 else PDBinfo[i],*matchObj.group(2,3,4), ' ' if len(layers) == 0 else layers[i]))
            i += 1
        if re.match(r' Rotational constants', line) or re.search(r'Distance matrix', line):
            Geo = 0
            i = 0
        if 'ONIOM: extrapolated energy' in line:
            E = line.split()[4]
            point.append(coords)
        if 'Step number' in line:
            step = re.search('Step number\s*(\d+)', line).group(1)
            if re.search('scan point\s*(\d+)', line):
                pointn = re.search('scan point\s*(\d+)', line).group(1)
            else:
                pointn = 0
            Title = str(E) + ' Hartree,' + ' Step ' + str(step) + ' of point ' + (str(pointn) if pointn != 0 else ' of Optimization')
            Head.append(Title)
            if pn + 1 == int(pointn):
                last = point.pop()
                points.append(point)
                point = [last]

                h =Head.pop()
                Heads.append(Head)
                Head = [h]

                pn = int(pointn)

print('Summary (If this is a Opitmization, it means that there is only one scan point!):')
for p, pt in enumerate(points, start=1):
    print('Scan point {:4}: {:4} steps'.format(p, len(pt)))

print('\nThe last structure, Step {}, point {}, will be extracted:'.format(len(points[-1]), len(points)))
try:
    print(re.search(r'(.* Hartree)', Heads[-1][-1]).group(1))
    print('\n*** Output file: GauMolStru.gjf ***\n')
    OUT = open('GauMolStru.gjf', mode='w')
    for s in settings:
        OUT.write(s + '\n')
    OUT.write(command + '\n')
    OUT.write('\n' + Heads[-1][-1]+'\n\n')
    OUT.write('Chg. Mul.\n')
    for x in points[-1][-1]:
        OUT.write(' {}  {:>15s}{:>15s}{:>15s} {}\n'.format(*x))
    OUT.close()
except IndexError:
    print('*** Warning! ***')
    print('It seems unnecessary to extract the last structure of this LOG file for the following resons.')
    print('  1. This calculation may be a single point energy calculation.')
    print('  2. This calculation is not completed, even for one step!')

