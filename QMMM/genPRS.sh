#!/bin/bash

# Usage: sh genPRS.sh TemplatePRS.gjf GauMolStru.gjf 64 66 -0.04 > proSc0_PRS.gjf
# -0.04 is the distance you want to change for C4N and H42

TempGjf=$1
targetGjf=$2
At1=$3
At2=$4
DeltaDist=$5

blankN=`sed -n '/^[ \t]*$/=' ${targetGjf} | sed -n '2p'`
N=$((${blankN}+2))
#echo $N

awk -v N=${N} -v temp=${TempGjf} -v atN1=${At1} -v atN2=${At2} -v target=${targetGjf} -v DeltaDist=${DeltaDist} 'function dist(a1, a2){
   return sqrt((a1[1]-a2[1])**2 + (a1[2]-a2[2])**2 + (a1[3]-a2[3])**2) }
BEGIN { i = 0; }
(NR >= N) && /PDBName/{
  i++;
  if(i == atN1 || i == atN2){
      match($0,/.* + (0|-1)? +-?[0-9]+\.[0-9]+ +-?[0-9]+\.[0-9]+ +-?[0-9]+\.[0-9]+/)
      partline = substr($0, RSTART, RLENGTH)
      Num = split(partline, a)
      coords[i] = (a[Num-2] " " a[Num-1] " " a[Num])
  }
}
END{
  i = 0;
  n = 1;
  split(coords[atN1],a1)
  split(coords[atN2],a2)
  Vec[1] = a2[1] - a1[1]
  Vec[2] = a2[2] - a1[2]
  Vec[3] = a2[3] - a1[3]
  newx = Vec[1]/dist(a1,a2) * (dist(a1,a2) + DeltaDist) + a1[1]
  newy = Vec[2]/dist(a1,a2) * (dist(a1,a2) + DeltaDist) + a1[2]
  newz = Vec[3]/dist(a1,a2) * (dist(a1,a2) + DeltaDist) + a1[3]
  while((getline line < temp)>0){
      if(line ~ /PDBName/){
          while(i != n){
              getline line1 < target
              if(line1 ~ /PDBName/){
                  match(line1,/.* + (0|-1)? +-?[0-9]+\.[0-9]+ +-?[0-9]+\.[0-9]+ +-?[0-9]+\.[0-9]+/)
                  New = substr(line1, RSTART, RLENGTH)
                  Num = split(New, atc)
                  if(i + 1 == atN2){
                      x = sprintf("%.6f", newx);
                      y = sprintf("%.6f", newy);
                      z = sprintf("%.6f", newz);
                  }else{
                      x=atc[Num-2];
                      y=atc[Num-1];
                      z=atc[Num];
                  }
                  i++
              }
          }
          num = split(line,lines)
          if(num==6) { printf(" %-53s%2s%14s%14s%14s%2s\n", lines[1],lines[2],x,y,z,lines[6])}
          if(num==8) { printf(" %-53s%2s%14s%14s%14s%2s%6s%4s\n", lines[1],lines[2],x,y,z,lines[6],lines[7],lines[8])}
          n++
      }else{
          print line
      }
  }
      #print coords[atN1], atN1;print coords[atN2], atN2
}
' ${targetGjf}


exit
