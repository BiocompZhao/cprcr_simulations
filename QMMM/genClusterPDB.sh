#!/bin/bash

prmtop=$1
RST=$2
outPDB=$3

cpptraj <<EOF > genClusterPDB.log
parm ${prmtop}
trajin ${RST}
reference ${RST}
autoimage
strip '!(:1|:44@HA,C,N,CA,CB,HB2,HB3,SG|:46@HA,C,N,CA,CB,HB2,HB3,OG,HG|:65@HA,C,N,CA,CB,HB2,HB3,CG,ND1,HD1,CE1,HE1,NE2,CD2,HD2|:154@HA,C,N,CA,CB,HB2,HB3,CG,OD1,OD2|:337|:339@C6N,H6N,C5N,H5N,C4N,H41,H42,C3N,C7N,O7N,N7N,H71,H72,C2N,H2N,N1N,C'N1,H'N1,C'N2,O'N4)'
trajout ${outPDB} pdb
EOF
