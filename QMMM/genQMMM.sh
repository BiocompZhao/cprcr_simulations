#!/bin/bash

TempGjf=$1
targetGjf=$2

awk -v target=${targetGjf} 'BEGIN {
            n=1;
        i=0
    }

/PDBName/ {
    while(i!=n) {
        getline line < target;
        if(line ~ /PDBName/){
            match(line,/.* + (0|-1)? +-?[0-9]+\.[0-9]+ +-?[0-9]+\.[0-9]+ +-?[0-9]+\.[0-9]+/)
            New = substr(line, RSTART, RLENGTH)
            Num = split(New, a)
            x=a[Num-2];
            y=a[Num-1];
            z=a[Num];
            i++
                }
    }
    if(NF==6) { printf(" %-53s%2s%14s%14s%14s%2s\n", $1,$2,x,y,z,$6)
    }
    if(NF==8) { printf(" %-53s%2s%14s%14s%14s%2s%6s%4s\n", $1,$2,x,y,z,$6,$7,$8)
    }
    n++
    next
    }

1 {
    print $0
  }' ${TempGjf}

