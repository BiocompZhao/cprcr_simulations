import sys
import matplotlib as mpl
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
import numpy as np

_, Figname = sys.argv

filepaths = ['%.3f/dist_%.3f.dat' % (i,i) for i in np.arange(5.5, 12, 0.075)]

fnt = {
        "family": "Liberation Sans",
        "stretch": "condensed"
        }
mpl.rc('font', **fnt)
mpl.rc('pdf', fonttype=42)
fig, ax = plt.subplots(figsize=(3.15,2.36))

for fpath in filepaths:
    dat = np.genfromtxt(fpath, dtype=[('Time', float), ('CV', float)], usecols=(0,2))
    ax.plot(dat['Time']/1000, dat['CV'], ls='None', marker='.', ms=0.25, mfc='black', mec='none')

ax.xaxis.set_major_locator(ticker.MultipleLocator(100))
ax.xaxis.set_minor_locator(ticker.MultipleLocator(50))
ax.yaxis.set_major_locator(ticker.MultipleLocator(2))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(1))
ax.set_ylim(5,12.5)
ax.set_xlim(0,550)
ax.set_xlabel('Times (ps)')
ax.set_ylabel(r"$Distance_{CA-C5} (\AA)$")

plt.savefig(Figname+'.png', dpi=600, bbox_inches='tight')
plt.savefig(Figname+'.pdf', dpi=600, bbox_inches='tight')
