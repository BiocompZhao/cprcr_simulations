#!/usr/bin/perl -w
use Cwd;
$wd=cwd;

print "Preparing input files\n";

my ($prmtopName, $rstName, $leftCV, $rightCV) = @ARGV;

$incr=0.075;
$force=50.0;

&prepare_input();

exit(0);

sub prepare_input() {
    
    $dist=$leftCV;
    while (sprintf("%.3f", $dist) <= $rightCV) { 
	$middle = sprintf("%.3f", ${dist});
	$dist = $middle;
	print "Processing distance: $dist\n";
	mkdir "$dist", 0755 or die "Cannot make $dist directory: $!";
	chdir "$dist" or die "Cannot chdir to $dist: $!";
	&write_mdin0();
	&write_mdin1();
	&write_mdin2();
	&write_disang();
	&write_batchfile();
    print "Running on distance: $dist ...\n";
	chmod 0755, "run_${dist}";
	system("./run_${dist} > run_${dist}.log");
	chdir "$wd" or die "Cannot chdir to $wd: $!";
	
      $dist += $incr;
    }
}

sub write_mdin0 {
    open MDINFILE,'>', "mdin_min_${dist}";
    print MDINFILE <<EOF;
5000 step minimization for ${dist} angstrom
 &cntrl
  imin = 1, 
  maxcyc=5000,
  ncyc = 2000,
  irest = 0,
  ntx = 1,
  ntpr = 1000,
  iwrap=1,
  cut = 10.0,
  nmropt = 1,
 &end
 &wt 
  type='END',
 &end
DISANG=dist_${dist}
EOF
    close MDINFILE;
}
sub write_mdin1 {
    open MDINFILE,'>', "mdin_equil_${dist}";
    print MDINFILE <<EOF;
50 ps NPT equilibration for ${dist} angstrom
 &cntrl
  imin = 0,
  ntx = 1,
  irest = 0,
  ntpr = 1000,
  ntwr = 1000,
  ntwx = 0,
  iwrap=1,
  ntf = 2,
  ntc = 2,
  cut = 10.0,
  ntb = 2,
  nstlim = 50000,
  dt = 0.001,
  temp0 = 300,
  ntt = 3,
  ig=-1,
  gamma_ln = 4.0,
  ntp = 1,
  pres0 = 1.0,
  nmropt = 1,
 &end
 &wt 
  type='END',
 &end
DISANG=dist_${dist}
EOF
    close MDINFILE;
}
sub write_mdin2 {
    open MDINFILE,'>', "mdin_prod_${dist}";
    print MDINFILE <<EOF;
550 ps NPT production for ${dist} angstrom
 &cntrl
  imin = 0,
  ntx = 5,
  irest = 1,
  iwrap=1,
  ntpr = 1000,
  ntwx = 4000,
  ntf = 2,
  ntc = 2,
  cut = 10.0,
  ntb = 2,
  nstlim = 550000,
  dt = 0.001,
  temp0 = 300.0,
  ntt = 3,
  gamma_ln = 4.0,
  ig = -1,
  ntp = 1,
  pres0 = 1.0,
  nmropt = 1,
 &end
 &wt
  type='DUMPFREQ', istep1=50,
 &end
 &wt 
  type='END',
 &end
DISANG=dist_${dist}
DUMPAVE=dist_${dist}.dat
EOF
    close MDINFILE;
}
sub write_disang {
    $left  = sprintf "%4.3f", 0;
    $min   = sprintf "%4.3f", ${dist};
    $right = sprintf "%4.3f", 99;
    open DISANG,'>', "dist_${dist}";
    print DISANG <<EOF;
Nonbonded model for Zinc and Harmonic restraints for $dist angstrom (umbrella sampling)
 &rst
  iat=6,5072
  r1=0.0000,
  r2=2.1044,
  r3=2.1044,
  r4=99.0000,
  rk2=22.6,
  rk3=22.6
 &end
 &rst
  iat=5,3866
  r1=$left,
  r2=$min,
  r3=$min,
  r4=$right,
  rk2=${force},
  rk3=${force},
 &end
EOF
    close DISANG;
}

sub write_batchfile {
    
    open BATCHFILE, '>', "run_${dist}";
    print BATCHFILE <<EOF;
#!/bin/bash


pmemd.cuda -O -i mdin_min_${dist} -p ../${prmtopName} -c ../${rstName} -r min_${dist}.rst  -o min_${dist}.out 
pmemd.cuda -O -i mdin_equil_${dist} -p ../${prmtopName} -c min_${dist}.rst  -r equil_${dist}.rst -o equil_${dist}.out 
pmemd.cuda -O -i mdin_prod_${dist} -p ../${prmtopName} -c equil_${dist}.rst -r prod_${dist}.rst -o prod_${dist}.out -x prod_${dist}.nc
EOF

#    print BATCHFILE "\necho \"Execution finished\"\n";
    close BATCHFILE;
}
