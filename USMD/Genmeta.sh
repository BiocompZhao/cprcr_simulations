#!/bin/bash

DIR=$1
if [ -e meta.dat ]
then
	rm -f meta.dat
fi
for i in `seq 5.5 0.075 12`
do
awk '{printf("%9s %9s\n",$1,$3)}' ./${i}/dist_${i}.dat > ${DIR}/dist_${i}.dat
echo "${DIR}/dist_${i}.dat ${i} 100.0" >> meta.dat
done

#wham 5.480 11.93 87 0.01 300 0 meta.dat free_tol0.01_energy.dat
#awk '(NR >= 2 && NR <= 88){print $1,$2}' free_tol0.01_energy.dat > pmf.dat

