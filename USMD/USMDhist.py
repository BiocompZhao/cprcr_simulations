import sys
import matplotlib as mpl
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
import numpy as np

_, Figname, MinCV, MaxCV = sys.argv

filepaths = ['%.3f/dist_%.3f.dat' % (i,i) for i in np.arange(5.5, 12, 0.075) if i <= float(MaxCV) and i >= float(MinCV)]
wins = [round(i, 3) for i in np.arange(5.5,12,0.075) if round(i,3) <= float(MaxCV) and round(i,3) >= float(MinCV)]
print("Windows plotted:", wins)
fnt = {
        "family": "Liberation Sans",
        "stretch": "condensed"
        }
mpl.rc('font', **fnt)
mpl.rc('pdf', fonttype=42)

fig, ax = plt.subplots(figsize=(3.15,2.36))
tickcolor=['black', 'black']
for fpath in filepaths:
    dat = np.genfromtxt(fpath, dtype=[('Time', float), ('CV', float)], usecols=(0,2))
    n,bins,ptch=ax.hist(dat['CV'], bins=100, facecolor='r', histtype='step', label='Window ' + fpath.split('/')[0])
    tickcolor.append(ptch[0].get_edgecolor())
tickcolor.extend(['black', 'black'])

ax.xaxis.set_major_locator(ticker.FixedLocator([wins[0]-0.15] + wins[::2] + [wins[-1]+0.075 if (len(wins)-1)%2 else wins[-1]+0.15]))
ax.xaxis.set_minor_locator(ticker.FixedLocator([wins[0]-0.075] + wins[1::2] + [wins[-1]+0.15 if (len(wins)-1)%2 else wins[-1]+0.075]))
majorTicks = ax.xaxis.get_major_ticks()
minorTicks = ax.xaxis.get_minor_ticks()
for i,c in enumerate(tickcolor):
    if i%2:
        plt.setp(minorTicks[i//2].tick1line, color = c, mew=1.5)
    else:
        plt.setp(majorTicks[i//2].tick1line, color = c, mew=1.5)

ax.yaxis.set_major_locator(ticker.MultipleLocator(100))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(50))
ax.set_xlim(wins[0] - 0.15, wins[-1] + 0.15)
ax.set_ylabel('Counts')
ax.set_xlabel(r"$Distance_{CA-C5} (\AA)$")

plt.savefig(Figname+'.png', dpi=600, bbox_inches='tight')
plt.savefig(Figname+'.pdf', dpi=600, bbox_inches='tight')
