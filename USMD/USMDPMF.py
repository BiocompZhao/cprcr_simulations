import sys
import matplotlib as mpl
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
import numpy as np

_, Figname, pmf = sys.argv

fnt = {
        "family": "Liberation Sans",
        "stretch": "condensed"
        }
mpl.rc('font', **fnt)
mpl.rc('pdf', fonttype=42)

dat = np.genfromtxt(pmf, dtype=[('CV', float), ('PMF', float)])

fig, ax = plt.subplots(figsize=(3.15,2.36))
ax.plot(dat['CV'], dat['PMF'], lw=1)

ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
ax.xaxis.set_minor_locator(ticker.MultipleLocator(0.2))
ax.yaxis.set_major_locator(ticker.MultipleLocator(1))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.2))

ax.set_xlim(5,12)
ax.set_ylabel('Potential of mean force (kcal/mol)')
ax.set_xlabel(r"$Distance_{CA-C5} (\AA)$")

plt.savefig(Figname+'.png', dpi=600, bbox_inches='tight')
plt.savefig(Figname+'.pdf', dpi=600, bbox_inches='tight')
